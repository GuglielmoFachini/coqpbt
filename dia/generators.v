Require Import QuickChick.
Require Import exp eval env.

Import QcDefaultNotation  GenLow GenHigh.
Open Scope qc_scope.

Definition maxVars := 5.
Definition genId := liftGen Id (choose (0, maxVars)).

Definition maxSize := 5.
Definition genSized {T: Type} (g: nat -> G T): G T := bindGen (choose (0, maxSize)) g.

Definition genAExp_ground := oneOf [
    liftGen ANum arbitrary;
    liftGen AId genId
].

Program Fixpoint genAExp_sized (h: nat) := match h with
  | O => genAExp_ground
  | S h' => freq [(1, genAExp_ground); (1, oneOf [
        liftGen2 APlus (genAExp_sized h') (genAExp_sized h');
        liftGen2 AMinus (genAExp_sized h') (genAExp_sized h');
        liftGen2 AMult (genAExp_sized h') (genAExp_sized h')
    ])]
end.

Definition genAExp := genSized genAExp_sized.

Program Fixpoint genBExp_sized (h: nat) := match h with
  | O => liftGen BBool arbitraryBool
  | S h' => freq [(1, liftGen BBool arbitraryBool); (1, oneOf [
        liftGen BNot (genBExp_sized h');
        liftGen2 BAnd (genBExp_sized h') (genBExp_sized h');
        liftGen2 BOr (genBExp_sized h') (genBExp_sized h');
        liftGen2 BEq (genAExp_sized h') (genAExp_sized h');
        liftGen2 BLe (genAExp_sized h') (genAExp_sized h')
    ])]
end.

Definition genBExp := genSized genBExp_sized.

Definition genCom_ground := oneOf [
    returnGen CSkip;
    liftGen2 CAss genId (genAExp_sized 0)
].

Program Fixpoint genCom_sized (h: nat) := match h with
  | O => genCom_ground
  | S h' => freq [(1, genCom_ground); (1, oneOf [
        liftGen2 CAss genId (genAExp_sized h');
        liftGen2 CSeq (genCom_sized h') (genCom_sized h');
        liftGen3 CIf (genBExp_sized h') (genCom_sized h') (genCom_sized h');
        liftGen2 CWhile (genBExp_sized h') (genCom_sized h')
    ])]
end.

Definition genCom := genSized genCom_sized.

Program Fixpoint genEnv_sized (h: nat) := match h with
  | O => returnGen empty_env
  | S h' => liftGen2 env_insert genId (genEnv_sized h')
end.

Definition genEnv := genSized genEnv_sized.

Program Fixpoint genState_sized (h: nat) := match h with
  | O => returnGen empty_state
  | S h' => liftGen3 update_state (genState_sized h') genId arbitrary
end.

Definition genState := genSized genState_sized.
