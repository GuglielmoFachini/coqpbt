Require Import QuickChick.
Require Import checks.

(* Extract Constant Test.defNumTests => "10". *)

QuickChick can_eval_aexp_with_initialized_vars.
QuickChick can_eval_bexp_with_initialized_vars.
QuickChick dia_preservation_from_state_env.
QuickChick dia_preservation_from_com_vars. (* FAILS *)
QuickChick ground_com_are_always_initialized.
QuickChick dia_progress_from_state_env.
QuickChick dia_progress_from_com_vars. (* FAILS *)
QuickChick dia_soundness.
