Require Import Arith.

Inductive id: Type := Id: nat -> id.

Definition same_id (x x': id): bool := match x, x' with Id n, Id n' => beq_nat n n' end.

Lemma eq_id_dec: forall (id1 id2: id), {id1 = id2} + {id1 <> id2}.
Proof.
   intros id1 id2.
   destruct id1 as [n1].
   destruct id2 as [n2].
   destruct (eq_nat_dec n1 n2) as [Heq | Hneq].
   - left. rewrite Heq. reflexivity.
   - right. intros contra. inversion contra. apply Hneq. apply H0.
Defined.

Inductive aexp: Type :=
  | ANum: nat -> aexp
  | AId: id -> aexp
  | APlus: aexp -> aexp -> aexp
  | AMinus: aexp -> aexp -> aexp
  | AMult: aexp -> aexp -> aexp.

Inductive bexp: Type :=
  | BBool: bool -> bexp
  | BNot: bexp -> bexp
  | BAnd: bexp -> bexp -> bexp
  | BOr: bexp -> bexp -> bexp
  | BEq: aexp -> aexp -> bexp
  | BLe: aexp -> aexp -> bexp.

Inductive com: Type :=
  | CSkip: com
  | CAss: id -> aexp -> com
  | CSeq: com -> com -> com
  | CIf: bexp -> com -> com -> com
  | CWhile: bexp -> com -> com.
