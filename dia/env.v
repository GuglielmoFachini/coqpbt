Require Import ListSet List.
Require Import exp.

Import ListNotations.

Definition env := ListSet.set id.

Definition empty_env: env := @empty_set id.

Definition env_insert (x: id) (e: env): env := @set_add id eq_id_dec x e.

Definition singleton_env (x: id): env := env_insert x empty_env.

Definition env_union (e e': env): env := @set_union id eq_id_dec e e'.

Definition env_intersection (e e': env): env := @set_inter id eq_id_dec e e'.

Definition in_env (x: id) (e: env) := @set_mem id eq_id_dec x e.

Fixpoint sub_env (e e': env): bool := match e with
  | [] => true
  | h :: t => match in_env h e' with
      | true => sub_env t e'
      | false => false
    end
end.

Fixpoint aexp_vars (a: aexp): env := match a with
  | ANum _ => empty_env
  | AId x => singleton_env x
  | APlus a' a'' | AMinus a' a'' | AMult a' a'' => env_union (aexp_vars a') (aexp_vars a'')
end.

Fixpoint bexp_vars (b: bexp): env := match b with
  | BBool _ => empty_env
  | BNot b' => bexp_vars b'
  | BAnd b' b'' | BOr b' b'' => env_union (bexp_vars b') (bexp_vars b'')
  | BEq a' a'' | BLe a' a'' => env_union (aexp_vars a') (aexp_vars a'')
end.
