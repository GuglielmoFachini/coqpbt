Require Import QuickChick String List.
Require Import exp eval.

Import ListNotations.

Open Scope string.

Instance show_id: Show id := {|
    show x := match x with Id n => "[ID " ++ show n ++ "]" end
|}.

Fixpoint aexp_to_string (a: aexp): string := match a with
  | ANum n => show n
  | AId x => show x
  | APlus a' a'' => "(" ++ aexp_to_string a' ++ " + " ++ aexp_to_string a'' ++ ")"
  | AMinus a' a'' => "(" ++ aexp_to_string a' ++ " - " ++ aexp_to_string a'' ++ ")"
  | AMult a' a'' => "(" ++ aexp_to_string a' ++ " * " ++ aexp_to_string a'' ++ ")"
end.

Instance show_aexp: Show aexp := {|
    show a := aexp_to_string a
|}.

Fixpoint bexp_to_string (b: bexp): string := match b with
  | BBool v => show v
  | BNot b' => "~" ++ bexp_to_string b' ++ ""
  | BAnd b' b'' => "(" ++ bexp_to_string b' ++ " && " ++ bexp_to_string b'' ++ ")"
  | BOr b' b'' => "(" ++ bexp_to_string b' ++ " || " ++ bexp_to_string b'' ++ ")"
  | BEq a' a'' => "(" ++ show a' ++ " == " ++ show a'' ++ ")"
  | BLe a' a'' => "(" ++ show a' ++ " <= " ++ show a'' ++ ")"
end.

Instance show_bexp: Show bexp := {|
    show b := bexp_to_string b
|}.

Fixpoint com_to_string (c: com): string := match c with
  | CSkip => "SKIP"
  | CAss x a => show x ++ " := " ++ show a
  | CSeq c' c'' => com_to_string c' ++ "; " ++ com_to_string c''
  | CIf b c' c'' => "IF (" ++ show b ++ ") { " ++ com_to_string c' ++ " } ELSE { " ++ com_to_string c'' ++ " }"
  | CWhile b c' => "WHILE (" ++ show b ++ ") { " ++ com_to_string c' ++ " }"
end.

Instance show_com: Show com := {|
    show c := com_to_string c
|}.

Fixpoint state_to_string (s: state): string := match s with
  | [] => ""
  | (Id i, n) :: s' => match s' with
      | [] => "[ID " ++ show i ++ " := " ++ show n ++ "]"
      | _ => "[ID " ++ show i ++ " := " ++ show n ++ "], " ++ state_to_string s'
    end
end.

Instance show_state: Show state := {|
    show s := "{" ++ state_to_string s ++ "}"
|}.

Close Scope string.
