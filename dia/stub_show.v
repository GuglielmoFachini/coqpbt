Require Import QuickChick String.
Require Import exp eval.

Open Scope string.

Definition warning := "currently using stub Show instances!".

Instance show_id: Show id := {|
    show x := warning
|}.

Instance show_aexp: Show aexp := {|
    show a := warning
|}.

Instance show_bexp: Show bexp := {|
    show b := warning
|}.

Instance show_com: Show com := {|
    show c := warning
|}.

Instance show_state: Show state := {|
    show s := warning
|}.

Close Scope string.
