Require Import QuickChick.
Require Import List String.
Require Import exp eval env dia model generators show.

Open Scope Checker_scope.

Definition has_value {T: Type} (o: option T) := match o with
  | Some _ => true
  | None => false
end.

Definition can_eval_aexp_with_initialized_vars: Checker :=
    forAll genState (fun s =>
    forAll genAExp (fun a =>
        sub_env (aexp_vars a) (state_env s) ==>
            (has_value (eval_aexp s a)))).

Definition can_eval_bexp_with_initialized_vars: Checker :=
    forAll genState (fun s =>
    forAll genBExp (fun b =>
        sub_env (bexp_vars b) (state_env s) ==>
            (has_value (eval_bexp s b)))).

Definition dia_preservation_from_state_env: Checker :=
    forAll genState (fun s =>
    forAll genCom (fun c =>
        match small_step s c with
          | None => label "trivial: command crashed" (true ==> true)
          | Some (s', c') =>
                is_initialized_in_env (state_env s) c ==>
                is_initialized_in_env (state_env s') c'
        end)).

Definition dia_preservation_from_com_vars: Checker :=
    forAll genState (fun s =>
    forAll genCom (fun c =>
        match small_step s c with
          | None => label "trivial: command crashed" (true ==> true)
          | Some (s', c') =>
                is_initialized_in_env (com_initialized_vars c) c ==>
                is_initialized_in_env (com_initialized_vars c') c'
        end)).

Fixpoint is_empty_env (e: env): bool := match e with
  | nil => true
  | _ => false
end.

Fixpoint is_ground_com (c: com): bool := match c with
  | CSkip => true
  | CAss x a => is_empty_env (aexp_vars a)
  | CSeq c' c'' => andb (is_ground_com c') (is_ground_com c'')
  | CIf b c' c'' => andb (is_empty_env (bexp_vars b)) (andb (is_ground_com c') (is_ground_com c''))
  | CWhile b c' => andb (is_empty_env (bexp_vars b)) (is_ground_com c')
end.

Definition ground_com_are_always_initialized: Checker :=
    forAll genCom (fun c =>
        is_ground_com c ==> is_initialized_in_env empty_env c).

Definition dia_progress_from_state_env: Checker :=
    forAll genState (fun s =>
    forAll genCom (fun c =>
        is_initialized_in_env (state_env s) c ==>
        classify (is_ground_com c) "trivial: ground command" (
            match c with
              | CSkip => true
              | _ => has_value (small_step s c)
            end))).

Definition dia_progress_from_com_vars: Checker :=
    forAll genState (fun s =>
    forAll genCom (fun c =>
        is_initialized_in_env (com_initialized_vars c) c ==>
        classify (is_ground_com c) "trivial: ground command" (
            has_value (small_step s c)))).

Definition dia_soundness: Checker :=
    forAll genState (fun s =>
    forAll genCom (fun c =>
        let r := small_step s c in
        has_value r ==>
        is_initialized_in_env (state_env s) c ==>
        match r with
          | Some (s', c') => has_value (small_step s' c')
          | None => false
        end)).
