Require Import exp env.

Fixpoint com_initialized_vars (c: com): env := match c with
  | CSkip => empty_env
  | CAss x _ => singleton_env x
  | CSeq c' c'' => env_union (com_initialized_vars c') (com_initialized_vars c'')
  | CIf _ c' c'' => env_intersection (com_initialized_vars c') (com_initialized_vars c'')
  | CWhile _ _ => empty_env
end.

Fixpoint dia (e: env) (c: com): option env := match c with
  | CSkip => Some e
  | CAss x a => if sub_env (aexp_vars a) e then Some (env_insert x e) else None
  | CSeq c' c'' => (match dia e c' with
      | Some e' => (match dia e' c'' with
          | Some e'' => Some e''
          | None => None
        end)
      | None => None
    end)
  | CIf b c' c'' => if sub_env (bexp_vars b) e then (match dia e c' with
      | Some e' => (match dia e c'' with
          | Some e'' => Some (env_intersection e' e'')
          | None => None
        end)
      | None => None
    end) else None
    | CWhile b c' => if sub_env (bexp_vars b) e then (match dia e c' with
        | Some _ => Some e
        | None => None
      end) else None
end.

Fixpoint is_initialized_in_env (e: env) (c: com): bool := match dia e c with
  | Some _ => true
  | None => false
end.

Definition is_definitely_initialized (c: com): bool := is_initialized_in_env empty_env c.
