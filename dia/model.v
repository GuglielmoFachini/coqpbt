Require Import List.
Require Import exp eval env.

Inductive definite_initialization: env -> com -> env -> Prop :=
  | DI_CSkip: forall (e: env),
        definite_initialization e CSkip e
  | DI_CAss: forall (e: env) (x: id) (a: aexp),
        sub_env (aexp_vars a) e = true ->
        definite_initialization e (CAss x a) (env_insert x e)
  | DI_CSeq: forall (e e' e'': env) (c c': com),
        definite_initialization e c e' ->
        definite_initialization e' c e'' ->
        definite_initialization e (CSeq c c') e''
  | DI_CIf: forall (e e' e'': env) (b: bexp) (c' c'': com),
        sub_env (bexp_vars b) e = true ->
        definite_initialization e c' e' ->
        definite_initialization e c'' e'' ->
        definite_initialization e (CIf b c' c'') (env_intersection e' e'')
  | DI_CWhile: forall (e e': env) (b: bexp) (c: com),
        sub_env (bexp_vars b) e = true ->
        definite_initialization e c e' ->
        definite_initialization e (CWhile b c) e.

Definition can_progress (s: state) (c: com) := match small_step s c with
  | Some _ => True
  | None => False
end.

Definition binding_variable (v: id * nat) := match v with (x, n) => x end.

Fixpoint state_env (s: state): env := map binding_variable s.

Definition dia_progress := forall (s: state) (c: com),
    exists (e: env), definite_initialization (state_env s) c e ->
    can_progress s c.

Definition dia_preservation := forall (s: state) (c: com),
    can_progress s c ->
    exists (e: env), definite_initialization (state_env s) c e ->
    exists (e': env), definite_initialization e c e'.
