Require Import Arith List.
Require Import exp.

Import ListNotations.

Definition state := list (id * nat).

Definition empty_state := @nil (id * nat).

Fixpoint update_state (s: state) (x: id) (n: nat): state := match s with
  | [] => [(x, n)]
  | (x', n') :: s' => if same_id x x' then s else (x', n') :: (update_state s' x n)
end.

Fixpoint var_value (s: state) (x: id): option nat := match s with
  | [] => None
  | (x', n') :: s' => if same_id x x' then Some n' else (var_value s' x)
end.

Definition lift_operator {X Y Z: Type} (op: X -> Y -> Z) (x: option X) (y: option Y): option Z := match x, y with
  | Some x', Some y' => Some (op x' y')
  | _, _ => None
end.

Fixpoint eval_aexp (s: state) (a: aexp): option nat := match a with
  | ANum n => Some n
  | AId i => var_value s i
  | APlus a' a'' => lift_operator plus (eval_aexp s a') (eval_aexp s a'')
  | AMinus a' a'' => lift_operator mult (eval_aexp s a') (eval_aexp s a'')
  | AMult a' a'' => lift_operator minus (eval_aexp s a') (eval_aexp s a'')
end.

Fixpoint eval_bexp (s: state) (b: bexp): option bool := match b with
  | BBool v => Some v
  | BNot b' => option_map negb (eval_bexp s b')
  | BAnd b' b'' => lift_operator andb (eval_bexp s b') (eval_bexp s b'')
  | BOr b' b'' => lift_operator orb (eval_bexp s b') (eval_bexp s b'')
  | BEq a' a'' => lift_operator beq_nat (eval_aexp s a') (eval_aexp s a'')
  | BLe a' a'' => lift_operator leb (eval_aexp s a') (eval_aexp s a'')
end.

Definition if_then_else {X: Type} (b: bool) (c c': X): X := match b with
  | true => c
  | false => c'
end.

Fixpoint small_step (s: state) (c: com): option (state * com) := match c with
  | CSkip => Some (s, c)
  | CAss x a => option_map (fun n => (update_state s x n, CSkip)) (eval_aexp s a)
  | CSeq CSkip c' => Some (s, c')
  | CSeq c' c'' => option_map (fun r => match r with (s', k) => (s', CSeq k c'') end) (small_step s c')
  | CIf b c' c'' => option_map (fun b' => (s, if_then_else b' c' c'')) (eval_bexp s b)
  | CWhile b c' => Some (s, CIf b (CSeq c' (CWhile b c')) CSkip)
end.
