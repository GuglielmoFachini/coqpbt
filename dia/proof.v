Require Import Bool QuickChick.
Require Import exp model env dia generators checks.

Lemma sub_env_union: forall (e e' e'': env),
    sub_env e' e = true ->
    sub_env e'' e = true ->
    sub_env (env_union e' e'') e = true.
Proof.
Admitted.

Lemma env_after_ass: forall (e e': env) (i: id) (a a': aexp),
    definite_initialization e (CAss i a) e' ->
    definite_initialization e (CAss i a') e'.
Proof.
Admitted.

Lemma aexp_env_init: forall (e: env) (i: id) (a: aexp),
    (exists e' : env, definite_initialization e (CAss i a) e') ->
    sub_env (aexp_vars a) e = true.
Proof.
intros.
induction a; try reflexivity.
- destruct H.
  inversion H.
  assumption.
- simpl.
  apply sub_env_union.
  + apply IHa1.
    inversion H.
    exists x.
    apply env_after_ass with (e:=e) (e':=x) (i:=i) (a':=a1) (a:=(APlus a1 a2)).
    assumption.
  + apply IHa2.
    inversion H.
    exists x.
    apply env_after_ass with (e:=e) (e':=x) (i:=i) (a':=a2) (a:=(APlus a1 a2)).
    assumption.
- simpl.
  apply sub_env_union.
  + apply IHa1.
    inversion H.
    exists x.
    apply env_after_ass with (e:=e) (e':=x) (i:=i) (a':=a1) (a:=(AMinus a1 a2)).
    assumption.
  + apply IHa2.
    inversion H.
    exists x.
    apply env_after_ass with (e:=e) (e':=x) (i:=i) (a':=a2) (a:=(AMinus a1 a2)).
    assumption.
- simpl.
  apply sub_env_union.
  + apply IHa1.
    inversion H.
    exists x.
    apply env_after_ass with (e:=e) (e':=x) (i:=i) (a':=a1) (a:=(AMult a1 a2)).
    assumption.
  + apply IHa2.
    inversion H.
    exists x.
    apply env_after_ass with (e:=e) (e':=x) (i:=i) (a':=a2) (a:=(AMult a1 a2)).
    assumption.
Qed.

Lemma dia_equivalence: forall (e: env) (c: com),
    reflect (exists (e': env), definite_initialization e c e')
            (has_value (dia e c)).
Proof.
intros.
apply iff_reflect.
split.
- intros.
  unfold has_value.
  + induction c; try reflexivity.
    * apply aexp_env_init in H.
      simpl.
      rewrite H. reflexivity.
    * inversion H.
(* We need to get PROH *)
Admitted.

Lemma dia_progress_checker_correct :
    semChecker dia_progress_from_state_env <-> dia_progress.
Proof.
Admitted.

Lemma dia_preservation_checker_correct :
    semChecker dia_preservation_from_state_env <-> dia_preservation.
Proof.
Admitted.
