Require Import ssreflect ssrnat ssrbool eqtype.

(* Formalization inspired from
   https://www.cs.princeton.edu/~appel/papers/redblack.pdf *)

(* An implementation of Red-Black Trees (insert only) *)

(* begin tree *)
Inductive color := Red | Black.
Inductive tree := Leaf : tree | Node : color -> tree -> nat -> tree -> tree.
(* end tree *)

(* insertion *)

Definition balance rb t1 k t2 :=
  match rb with
    | Red => Node Red t1 k t2
    | _ =>
      match t1 with
        | Node Red (Node Red a x b) y c =>
          Node Red (Node Black a x b) y (Node Black c k t2)
        | Node Red a x (Node Red b y c) =>
          Node Red (Node Black a x b) y (Node Black c k t2)
        | a => match t2 with
                 | Node Red (Node Red b y c) z d =>
                   Node Red (Node Black t1 k b) y (Node Black c z d)
                 | Node Red b y (Node Red c z d) =>
                   Node Red (Node Black t1 k b) y (Node Black c z d)
                 | _ => Node Black t1 k t2
               end
      end
  end.

Fixpoint ins x s :=
  match s with
    | Leaf => Node Red Leaf x Leaf
    | Node c a y b => if x < y then balance c (ins x a) y b
                      else if y < x then balance c a y (ins x b)
                           else Node c a x b
  end.

Definition makeBlack t :=
  match t with
    | Leaf => Leaf
    | Node _ a x b => Node Black a x b
  end.

Definition insert x s := makeBlack (ins x s).


(* Red-Black Tree invariant: declarative definition *)
(* begin is_redblack *)
Inductive is_redblack' : tree -> color -> nat -> Prop :=
| IsRB_leaf: forall c, is_redblack' Leaf c 0
| IsRB_r: forall n tl tr h, is_redblack' tl Red h -> is_redblack' tr Red h ->
                            is_redblack' (Node Red tl n tr) Black h
| IsRB_b: forall c n tl tr h, is_redblack' tl Black h -> is_redblack' tr Black h ->
                              is_redblack' (Node Black tl n tr) c (S h).
Definition is_redblack (t:tree) : Prop := exists h, is_redblack' t Red h.
(* end is_redblack *)

(* begin insert_preserves_redblack *)
Definition insert_preserves_redblack : Prop :=
  forall x s, is_redblack s -> is_redblack (insert x s).
(* end insert_preserves_redblack *)

(* Declarative Proposition *)
Lemma insert_preserves_redblack_correct : insert_preserves_redblack.
Abort. (* if this wasn't about testing, we would just prove this *)


(*=======================================================*)
(*                        CHECKS                         *)
(*=======================================================*)

Require Import QuickChick.
Require Import GenLow GenHigh.
Import GenLow GenHigh.
Require Import NPeano.

Require Import ssreflect ssrnat ssrbool eqtype.

Require Import List String.
Import ListNotations.

Open Scope string.

Open Scope Checker_scope.

(* Red-Black Tree invariant: executable definition *)

Fixpoint black_height_bool (t: tree) : option nat :=
  match t with
    | Leaf => Some 0
    | Node c tl _ tr =>
      let h1 := black_height_bool tl in
      let h2 := black_height_bool tr in
      match h1, h2 with
        | Some n1, Some n2 =>
          if n1 == n2 then
            match c with
              | Black => Some (S n1)
              | Red => Some n1
            end
          else None
        | _, _ => None
      end
  end.

Definition is_black_balanced (t : tree) : bool :=
  isSome (black_height_bool t).

Fixpoint has_no_red_red (c : color) (t : tree) : bool :=
  match t with
    | Leaf => true
    | Node Red t1 _ t2 =>
      match c with
        | Red => false
        | Black => has_no_red_red Red t1 && has_no_red_red Red t2
      end
    | Node Black t1 _ t2 =>
      has_no_red_red Black t1 && has_no_red_red Black t2
  end.

(* begin is_redblack_bool *)
Definition is_redblack_bool (t : tree) : bool :=
  is_black_balanced t && has_no_red_red Red t.
(* end is_redblack_bool *)

Fixpoint showColor (c : color) :=
  match c with
    | Red => "Red"
    | Black => "Black"
  end.

Fixpoint tree_to_string (t : tree) :=
  match t with
    | Leaf => "Leaf"
    | Node c l x r => "Node " ++ showColor c ++ " "
                            ++ "(" ++ tree_to_string l ++ ") "
                            ++ show x ++ " "
                            ++ "(" ++ tree_to_string r ++ ")"
  end.

Instance showTree {A : Type} `{_ : Show A} : Show tree :=
  {|
    show t := "" (* CH: tree_to_string t causes a 9x increase in runtime *)
  |}.

(* begin insert_preserves_redblack_checker *)
Definition insert_preserves_redblack_checker (genTree : G tree) : Checker :=
  forAll arbitrary (fun n => forAll genTree (fun t =>
    is_redblack_bool t ==> is_redblack_bool (insert n t))).
(* end insert_preserves_redblack_checker *)

Import QcDefaultNotation. Open Scope qc_scope.

(* begin genAnyTree *)
Definition genColor := elems [Red; Black].
Fixpoint genAnyTree_depth (d : nat) : G tree :=
  match d with
    | 0 => returnGen Leaf
    | S d' => freq [(1, returnGen Leaf);
                    (9, liftGen4 Node genColor (genAnyTree_depth d')
                                     arbitrary (genAnyTree_depth d'))]
  end.
Definition genAnyTree : G tree := sized genAnyTree_depth.
(* end genAnyTree *)

Extract Constant defSize => "10".
(* begin QC_naive *)
QuickChick (insert_preserves_redblack_checker genAnyTree).
(* end QC_naive *)

(* gathering some size statistics *)
Fixpoint tree_size (t : tree) : nat :=
  match t with
    | Leaf => 1
    | Node c tl _ tr => 1 + (tree_size tl) + (tree_size tr)
  end.

Definition insert_preserves_redblack_checker_size (genTree : G tree) : Checker :=
  forAll arbitrary (fun n => forAll genTree (fun t =>
    collect (append "size " (show (tree_size t)))
    (is_redblack_bool t ==> is_redblack_bool (insert n t)))).

(*
Extract Constant Test.defNumTests => "100000".
QuickChick (insert_preserves_redblack_checker_size genAnyTree).
*)

Module DoNotation.
Import ssrfun.
Notation "'do!' X <- A ; B" :=
  (bindGen A (fun X => B))
  (at level 200, X ident, A at level 100, B at level 200).
End DoNotation.
Import DoNotation.

Require Import Relations Wellfounded Lexicographic_Product.

Definition ltColor (c1 c2: color) : Prop :=
  match c1, c2 with
    | Red, Black => True
    | _, _ => False
  end.

Lemma well_foulded_ltColor : well_founded ltColor.
Proof.
  unfold well_founded.
  intros c; destruct c;
  repeat (constructor; intros c ?; destruct c; try now (exfalso; auto)).
Qed.

Definition sigT_of_prod {A B : Type} (p : A * B) : {_ : A & B} :=
  let (a, b) := p in existT (fun _ : A => B) a b.

Definition prod_of_sigT {A B : Type} (p : {_ : A & B}) : A * B :=
  let (a, b) := p in (a, b).


Definition wf_hc (c1 c2 : (nat * color)) : Prop :=
  lexprod nat (fun _ => color) lt (fun _ => ltColor) (sigT_of_prod c1) (sigT_of_prod c2).

Lemma well_founded_hc : well_founded wf_hc.
Proof.
  unfold wf_hc. apply wf_inverse_image.
  apply wf_lexprod. now apply Wf_nat.lt_wf. intros _; now apply well_foulded_ltColor.
Qed.

Require Import Program.Wf. Import WfExtensionality.
Require Import FunctionalExtensionality.

(* begin genRBTree_height *)
Program Fixpoint genRBTree_height (hc : nat*color) {wf wf_hc hc} : G tree :=
  match hc with
  | (0, Red) => returnGen Leaf
  | (0, Black) => oneOf [returnGen Leaf;
                    (do! n <- arbitrary; returnGen (Node Red Leaf n Leaf))]
  | (S h, Red) => liftGen4 Node (returnGen Black) (genRBTree_height (h, Black))
                                        arbitrary (genRBTree_height (h, Black))
  | (S h, Black) => do! c' <- genColor;
                    let h' := match c' with Red => S h | Black => h end in
                    liftGen4 Node (returnGen c') (genRBTree_height (h', c'))
                                       arbitrary (genRBTree_height (h', c')) end.
(* end genRBTree_height *)
Next Obligation.
  abstract (unfold wf_hc; simpl; left; omega).
Qed.
Next Obligation.
  abstract (unfold wf_hc; simpl; left; omega).
Qed.
Next Obligation.
  abstract (unfold wf_hc; simpl; destruct c'; [right; apply I | left; omega]).
Qed.
Next Obligation.
  abstract (unfold wf_hc; simpl; destruct c'; [right; apply I | left; omega]).
Qed.
Next Obligation.
  abstract (apply well_founded_hc).
Defined.

Lemma genRBTree_height_eq (hc : nat*color) :
  genRBTree_height hc =
  match hc with
  | (0, Red) => returnGen Leaf
  | (0, Black) => oneOf [returnGen Leaf;
                    (do! n <- arbitrary; returnGen (Node Red Leaf n Leaf))]
  | (S h, Red) => liftGen4 Node (returnGen Black) (genRBTree_height (h, Black))
                                        arbitrary (genRBTree_height (h, Black))
  | (S h, Black) => do! c' <- genColor;
                    let h' := match c' with Red => S h | Black => h end in
                    liftGen4 Node (returnGen c') (genRBTree_height (h', c'))
                                       arbitrary (genRBTree_height (h', c')) end.
Proof.
  unfold_sub genRBTree_height (genRBTree_height hc).
  f_equal. destruct hc as [[|h] [|]]; try reflexivity.
  f_equal. apply functional_extensionality => [[|]]; reflexivity.
Qed.

(* Hope that this is enough for preventing unfolding genRBTree_height *)
Global Opaque genRBTree_height.


(* begin genRBTree *)
Definition genRBTree := bindGen arbitrary (fun h => genRBTree_height (h, Red)).
(* end genRBTree *)

Definition showDiscards (r : Result) :=
  match r with
  | Success ns nd _ _ => "Success: number of successes " ++ show (ns-1) ++ newline ++
                         "         number of discards "  ++ show nd ++ newline
  | _ => show r
  end.

Definition testInsert :=
  showDiscards (quickCheck (insert_preserves_redblack_checker genRBTree)).

Extract Constant defSize => "10".
(* begin QC_good *)
QuickChick (insert_preserves_redblack_checker genRBTree).
(* end QC_good *)

(* gathering some size statistics
Extract Constant Test.defNumTests => "100000".
QuickChick (insert_preserves_redblack_checker_size genRBTree).
*)


(*==============================================================*)
(*                              PROOFS                          *)
(*==============================================================*)


Require Import ssreflect ssrnat ssrbool eqtype.
Require Import List String.
Require Import QuickChick.
Import GenLow GenHigh.

(* correspondence between the inductive and the executable definitions *)
Lemma has_black_height :
  forall t h c, is_redblack' t c h -> black_height_bool t = Some h.
Proof.
  elim => [| c t1 IHt1 n t2 IHt2] h c' Hrb; first by inversion Hrb.
  inversion Hrb as [| n' tl tr h' Htl Htr | c'' n' tl tr h' Htl Htr]; subst;
  move: Htl Htr => /IHt1 Htl /IHt2 Htr; simpl; by rewrite Htl Htr eq_refl.
Qed.

Lemma is_redblack'P :
  forall (t : tree) n c,
    reflect (is_redblack' t c n)
            ((black_height_bool t == Some n) && has_no_red_red c t).
Proof.
  elim => [| c t1 IHt1 n t2 IHt2] n' c'.
  - simpl.
    apply (@iffP ((Some 0 == Some n') && true));
    first by apply/idP.
    + move => /andP [/eqP [H1] _]; subst.
      econstructor.
    + move => Hrb. apply/andP. inversion Hrb; subst. split => //.
  - apply (@iffP ((black_height_bool (Node c t1 n t2) == Some n') &&
                   has_no_red_red c' (Node c t1 n t2)));
    first by apply/idP.
    + move => /andP [/eqP /= H1 H2]; subst.
      destruct (black_height_bool t1) eqn:Heqh1,
               (black_height_bool t2) eqn:Heqh2; (try discriminate).
      have Heq : (n0 = n1) by apply/eqP; destruct (n0 == n1). subst.
      rewrite eq_refl in H1.
      destruct c; inversion H1; subst; clear H1;
      destruct c' => //=; move : H2 => /andP [Ht1 Ht2];
      (constructor; [apply/IHt1 | apply/IHt2]); apply/andP; split => //.
    + move => Hrb.
      inversion Hrb as [| n'' tl tr h Hrbl Hrbr | c'' n'' tl tr h Hrbl Hrbr]; subst;
      move: Hrbl Hrbr => /IHt1/andP [/eqP Hbhl Hrrl] /IHt2/andP [/eqP Hbhr Hrrr];
      apply/andP; split => //; simpl; (try by rewrite Hbhl Hbhr eq_refl);
      by (apply/andP; split => //).
Qed.

(* begin is_redblackP *)
Lemma is_redblackP t : reflect (is_redblack t) (is_redblack_bool t).
(* end is_redblackP *)
Proof.
  apply (@iffP (is_redblack_bool t)); first by apply/idP.
  rewrite /is_redblack_bool.
  + move => /andP [Hb Hrr]. rewrite /is_black_balanced in Hb.
    have [h Hbh] : exists h, black_height_bool t = Some h
      by destruct (black_height_bool t) => //; eexists.
    exists h. apply/is_redblack'P. apply/andP; split => //; apply/eqP => //.
  + move => [h /is_redblack'P /andP [/eqP H1 H2]].
    rewrite /is_redblack_bool /is_black_balanced H1. apply/andP; split => //.
Qed.

(* begin semColor *)
Lemma semColor : semGen genColor <--> [set : color].
(* end semColor *)
Proof.
  rewrite /genColor. rewrite semElements.
  intros c. destruct c; simpl; unfold setT; tauto.
Qed.

Corollary genColor_correctSize': forall s, semGenSize genColor s <--> setT.
Proof.
  move => s. rewrite unsized_alt_def. by apply semColor.
Qed.

Instance genRBTree_heightMonotonic p :
  SizeMonotonic (genRBTree_height p).
Proof.
  move : p.
  eapply (well_founded_induction well_founded_hc).
  move => [[|n] c] IH; rewrite genRBTree_height_eq.
  - case : c {IH}; eauto with typeclass_instances.
    apply oneofMonotonic; eauto with typeclass_instances.
    move => t [H1 | [H2 | //]]; subst; eauto with typeclass_instances.
  - case : c IH => IH. apply liftGen4Monotonic; eauto with typeclass_instances;
    eapply IH; eauto; by constructor; omega.
    apply bindMonotonic; eauto with typeclass_instances.
    move => x /=. apply liftGen4Monotonic; eauto with typeclass_instances;
    eapply IH; eauto; (case : x; [ by right | by left; omega]).
Qed.

Instance genRBTreeMonotonic : SizeMonotonic genRBTree.
Proof.
  apply bindMonotonic; eauto with typeclass_instances.
Qed.

(* begin semGenRBTreeHeight *)
Lemma semGenRBTreeHeight h c :
  semGen (genRBTree_height (h, c)) <--> [set t | is_redblack' t c h ].
(* end semGenRBTreeHeight *)
Proof.
  replace c with (snd (h, c)); replace h with (fst (h, c)); try reflexivity.
  move : (h, c). clear h c.
  eapply (well_founded_induction well_founded_hc).
  move => [[|h] []] IH /=; rewrite genRBTree_height_eq.
  - rewrite semReturn. split. move => <-. constructor.
    move => H. inversion H; subst; reflexivity.
  - rewrite semOneof. move => t. split.
    move => [gen [[H1 | [H1 | // _]] H2]]; subst.
    apply semReturn in H2. rewrite - H2. constructor.
    move : H2 => /semBindSizeMonotonic. move => [n [_ /semReturn <-]].
    constructor. constructor. constructor.
    move => H. inversion H; subst.
    { eexists. split. left. reflexivity. inversion H; subst.
        by apply semReturn. }
    { inversion H0; subst. inversion H1; subst.
      eexists. split. right. left. reflexivity.
      apply semBindSizeMonotonic; eauto with typeclass_instances.
      eexists. split; last by apply semReturn; reflexivity.
      by apply arbNat_correct. }
  - rewrite semLiftGen4SizeMonotonic. split.
    + move => /= [c [t1 [n [t2 [/semReturn H1 [H2 [H3 [H4 H5]]]]]]]].
      rewrite <- H1 in *. clear H1. subst.
      apply IH in H2; last by left; omega.
      apply IH in H4; last by left; omega. constructor; eauto.
    + move => H. inversion H; subst.
      apply (IH (h, Black)) in H1; last by left; omega.
      apply (IH (h, Black)) in H4; last by left; omega.
      eexists. eexists. eexists. eexists. repeat (split; auto; try reflexivity).
      by apply semReturn. by auto.
      by apply arbNat_correct. by auto.
  - rewrite semBindSizeMonotonic /=. split.
    + move => [c [_ /= /semLiftGen4SizeMonotonic
                    [c' [t1 [n [t2 [/semReturn H1 [H2 [_ [H4 H5]]]]]]]]]].
      rewrite <- H1 in *. clear H1. subst. destruct c.
      apply IH in H2; last by right.
      apply IH in H4; last by right. simpl in *.
      constructor; eauto.
      apply IH in H2; last by left; omega.
      apply IH in H4; last by left; omega. constructor; eauto.
    + move => H. inversion H; subst.
      apply (IH (h.+1, Red)) in H0; last by right.
      apply (IH (h.+1, Red)) in H1; last by right.
      eexists Red. split; first by apply semColor.
      apply semLiftGen4SizeMonotonic; eauto with typeclass_instances.
      eexists. eexists. eexists. eexists. repeat (split; auto; try reflexivity).
      by apply semReturn. by auto.
      by apply arbNat_correct. by auto.
      apply (IH (h, Black)) in H1; last by left; omega.
      apply (IH (h, Black)) in H4; last by left; omega.
      eexists Black. split; first by apply semColor.
      apply semLiftGen4SizeMonotonic; eauto with typeclass_instances.
      eexists. eexists. eexists. eexists. repeat (split; auto; try reflexivity).
      by apply semReturn. by auto.
      by apply arbNat_correct. by auto.
Qed.


(* begin semRBTree *)
Lemma semRBTree : semGen genRBTree <--> [set t | is_redblack t].
(* end semRBTree *)
Proof.
  rewrite /genRBTree /is_redblack.
  rewrite semBindSizeMonotonic. setoid_rewrite semGenRBTreeHeight.
  move => t. split.
  - move => [n [_ H2]].  eexists; eauto.
  - move => [n H3].  eexists. split; eauto.
    by apply arbNat_correct.
Qed.

(* begin insert_preserves_redblack_checker_correct *)
Lemma insert_preserves_redblack_checker_correct:
  semChecker (insert_preserves_redblack_checker genRBTree)
  <-> insert_preserves_redblack.
(* end insert_preserves_redblack_checker_correct *)
Proof.
  rewrite (mergeForAlls arbitraryNat genRBTree).
  rewrite -> semForAllUnsized2.
  rewrite /genPair. split.
  - move => H n t irt. specialize (H (n,t)). simpl in H.
    rewrite /semCheckable in H. simpl in H. rewrite -> semImplication in H.
    rewrite -> semCheckableBool in H.
    apply /is_redblackP. apply : H.
    apply semLiftGen2SizeMonotonic; eauto with typeclass_instances.
    exists (n, t). split => //. split => //. by apply arbNat_correct.
    by apply semRBTree. by apply/is_redblackP.
  - move => H [a t] /semLiftGen2SizeMonotonic [[n t'] [[_ Hg] [<- <-]]].
    simpl. rewrite -> semImplication. move => Hb. rewrite semCheckableBool.
    apply /is_redblackP. apply H. by apply /is_redblackP.
  - simpl. eauto with typeclass_instances.
Qed.

Lemma insert_preserves_redblack_checker_correct' :
  semChecker (insert_preserves_redblack_checker genRBTree)
  <-> insert_preserves_redblack.
Proof.
  rewrite /insert_preserves_redblack_checker /insert_preserves_redblack.
  rewrite -> semForAllSizeMonotonic; try by eauto with typeclass_instances.
  - split.
    + move => H n t irt.
      have HH : semGen arbitraryNat n by (apply arbNat_correct; reflexivity).
      specialize (H n HH).
      rewrite -> semForAllSizeMonotonic in H;
        try by (try move => ? /=); auto with typeclass_instances.
      specialize (H t).
      rewrite -> (semRBTree t) in H. simpl in H. specialize (H irt).
      rewrite -> semImplication in H. apply /is_redblackP.
      rewrite -> semCheckableBool in H. apply H. by apply /is_redblackP.
    + move => H a _ /=. rewrite -> semForAllSizeMonotonic;
        try by (try move => ? /=); auto with typeclass_instances.
      move => t Hg. rewrite -> semImplication => Hrb.
      rewrite semCheckableBool. apply /is_redblackP; apply H.
        by apply /is_redblackP.
  - move => n /=. apply forAllMonotonic;
      try by (try move => ? /=); auto with typeclass_instances.
Qed.
