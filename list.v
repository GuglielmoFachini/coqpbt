Require Import QuickChick.
Require Import List seq ssreflect ssrbool ssrnat ZArith eqtype.
Import ListNotations.
Import GenLow GenHigh.
Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

(****************************************************************************)
(* length of a list *)

Fixpoint length (l : list nat) : nat :=
  match l with
  | [] => 0
  | _ :: l' => 1 + length l'
  end.

Definition prop_lengthIsCorrect (l : list nat) :=
  length l == List.length l.

QuickChick prop_lengthIsCorrect.

(****************************************************************************)
(* reverse list *)

Fixpoint snoc (x : nat) (xs : list nat) :=
  match xs with
  | [] => [x]
  | y :: ys => y :: snoc x ys
  end.

Fixpoint reverse (l : list nat) : list nat :=
  match l with
  | [] => []
  | hd :: tl => snoc hd (reverse tl)
  end.

Definition prop_reverseIsInvolutive (xs : list nat) :=
  reverse (reverse xs) == xs.

QuickChick prop_reverseIsInvolutive.

(****************************************************************************)
(* insert in an ordered list *)

Fixpoint ordered (l : list nat) : bool :=
  match l with
  | [] => true
  | x :: rest => match rest with
                 | [] => true
                 | y :: ys => (x <= y) && ordered rest
                 end
  end.

Fixpoint insert (x : nat) (xs : list nat) : list nat :=
  match xs with
  | [] => [x]
  | y :: ys => if x <= y then
                 x :: xs
               else
                 y :: insert x ys
  end.

Definition prop_insertPreservesOrder (x : nat) (xs : list nat) :=
  collect (List.length xs) (ordered xs ==> ordered (insert x xs)).

QuickChick prop_insertPreservesOrder.

(****************************************************************************)

(* Bugged definition of remove *)
(* remove x l deletes the first occurence of x in l *)

Fixpoint remove (x : nat) (l : list nat) : list nat :=
  match l with
  | []   => []
  | h::t => if beq_nat h x then t else h :: remove x t
  end.

Definition prop_remove (x : nat) (l : list nat) :=
  collect x (~~ (existsb (pred1 x) (remove x l))).

QuickChick prop_remove.

(* Proving correctness of the checker prop_remove *)

Definition max_elem :=
  fix f l :=
    match l with
    | [] => 0
    | x :: xs => (max x (f xs))
    end.

Lemma below_max_elem:
  forall (l : list nat) x,
    In x l -> x <= (max_elem l).
Proof.
  intros l.
  elim : l => //= [x1 xs IHxs] x2 [Heq | HIn]; subst.
  - apply/leP. by apply Max.le_max_l.
  - apply IHxs in HIn. apply Max.max_case_strong => H; auto.
    apply/leP. eapply le_trans; try eassumption. by apply/leP.
Qed.

Theorem prop_remove_correct:
  semCheckable prop_remove <-> (forall (x : nat) l, ~ In x (remove x l)).
Proof.
  unfold semCheckable, semChecker. setoid_rewrite <- proposition_equiv.
  simpl; split. unfold prop_remove.
  - move => H x l cont.
    set size := max x (max (max_elem l) (List.length l)).
    have Hnat : semGenSize arbitraryNat size x
      by apply arbNat_correctSize; apply Max.le_max_l.
    have Hlist: semGenSize arbitraryList size l.
    { eapply arbList_correct with (P := fun x y => (y <= x)%coq_nat).
      move => n. by rewrite (arbNat_correctSize _ _).
      split. apply Max.max_case_strong => H'; auto. apply/leP.
      eapply Max.max_lub_r; eassumption. by apply/leP; apply Max.le_max_r.
      move => x' /below_max_elem /leP Helem.
      apply Max.max_case_strong => H'; auto.
      eapply le_trans; try eassumption. eapply Max.max_lub_l. by eapply H'.
      apply Max.max_case_strong => H''; auto.
      eapply le_trans; try eassumption. }
    specialize (H size x Hnat l Hlist).
    setoid_rewrite semCollect_idSize in H. apply semCheckableBoolSize in H.
    have contra : existsb (pred1 x) (remove x l).
      { apply existsb_exists. exists x. split => //. by rewrite /= eq_refl. }
    rewrite contra in H. discriminate.
  -  move => H a HIn Hsize l Hsize'.
     rewrite /prop_remove. rewrite semCollect_idSize semCheckableBoolSize. apply Bool.eq_true_not_negb.
     move => /existsb_exists contra.
     move : contra => [n [HIn' /=/eqP Hpred]]; subst. eapply H.
     eassumption.
Qed.